<?php
	//$url = 'http://www.sbstransit.com.sg/iris_api/nextbus.aspx';
	$url = 'http://203.127.121.105/iris_api/nextbus.aspx';

	$iris_key = 'D3F15661-D54E-4A89-990B-7EE81B8B3AA3';
	$user_agent = 'SBSTransitIris/81 CFNetwork/548.1.4 Darwin/11.0.0';
	$cookie = 'rl-sticky-key=c0a8089c';
	$encoding = 'gzip, deflate';
	//$accept = '*/*';
	//$accept_encoding = 'ja-jp';

	$bus_num = $_GET['bus_num'];
	$bus_stop = $_GET['bus_stop'];

	if (empty($bus_num) || empty($bus_stop)) exit;

	$url .= '?svc='.$bus_num;
	$url .= '&busstop='.$bus_stop;
	$url .= '&iriskey='.$iris_key;

	$process = curl_init($url);
	curl_setopt($process, CURLOPT_HEADER, 0);
	curl_setopt($process, CURLOPT_USERAGENT, $user_agent);
	curl_setopt($process, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($process, CURLOPT_ENCODING , $encoding);
	curl_setopt($process, CURLOPT_TIMEOUT, 30);
	curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
	$return = curl_exec($process);
	curl_close($process);

	if ($return === FALSE) {
		$result = array(
			'success' => FALSE,
			'next' => 999,
			'subsequent' => 999
		);
	} else {
		$pos_start = strpos($return, '<nextbus><t>') + strlen('<nextbus><t>');
		$pos_end   = strpos($return, '</t><wab>', $pos_start) - $pos_start;
		$nextbus   = substr($return, $pos_start, $pos_end);

		$pos_start = strpos($return, '<subsequentbus><t>') + strlen('<subsequentbus><t>');
		$pos_end   = strpos($return, '</t><wab>', $pos_start) - $pos_start;
		$subsbus   = substr($return, $pos_start, $pos_end);

		$result = array(
			'success' => TRUE,
			'next' => $nextbus,
			'subsequent' => $subsbus
		);
	}

	header('Content-type: application/json');
	//header('Access-Control-Allow-Origin: *');
	//header('Access-Control-Allow-Methods: POST, GET');
	//header('Access-Control-Allow-Headers: *');
	//header('Access-Control-Max-Age: 1728000');
	echo json_encode($result);

	//echo '<div style="margin-top:200px;"><textarea style="width:800px;height:200px;">',  mysql_escape_string($return),'</textarea></div>';
	//<iris><result><service_no>262</service_no><nextbus><t>11</t><wab>0</wab></nextbus><subsequentbus><t>24</t><wab>0</wab></subsequentbus></result></iris>1