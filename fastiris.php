<?php
	$buses = array(
		array(
			'num' => 262,
			'desc' => 'Near Home',
			'stop' => '54621',
			'cat' => 'to_work',
		),
		array(
			'num' => 133,
			'desc' => 'Near Home',
			'stop' => '54071',
			'cat' => 'to_work',
		),
		array(
			'num' => 133,
			'desc' => 'Synergy Hub',
			'stop' => '03222',
			'cat' => 'from_work',
		),
		array(
			'num' => 262,
			'desc' => 'Near Home',
			'stop' => '54099',
			'cat' => 'from_work',
		),
		array(
			'num' => 166,
			'desc' => 'Opposite Mayflower',
			'stop' => '54219',
			'cat' => '',
		),
		array(
			'num' => 80,
			'desc' => 'Opp Funan',
			'stop' => '04249',
			'cat' => '',
		),
		array(
			'num' => 145,
			'desc' => 'Opp Funan',
			'stop' => '04249',
			'cat' => '',
		),
		array(
			'num' => 132,
			'desc' => 'Opposite Mayflower',
			'stop' => '54219',
			'cat' => '',
		),
		array(
			'num' => 132,
			'desc' => 'Thai Embassy',
			'stop' => '09179',
			'cat' => '',
		),
		array(
			'num' => 166,
			'desc' => 'Clark Quay Central',
			'stop' => '04222',
			'cat' => '',
		),
	);
?><html>
	<head>
		<title>FastIris</title>
		<style>
			* {
				font-family: 'HelveticaNeue-Light', 'Verdana', sans-serif;
				font-size: 13px;
				color: #333;
			}
			a {
				text-decoration: none;
			}
			table {
				border-collapse: collapse;
			}
			.clearfix {
				clear: both;
			}
			.time {
				font-size: 21px;
				color: #666;
			}
			.bus-num {
				font-family: 'HelveticaNeue-UltraLight';
				font-size: 36px;
			}
			.description {
				padding: 5px 0 0 5px;
			}
			.bus {
				border-bottom: solid 1px #DDD;
			}
		</style>
		<script src="/jquery-1.7.2.min.js"></script>

		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="viewport" content="width=device-width,user-scalable=no">
	</head>
	<body>
		<?php foreach($buses AS $b) { ?>
			<div class="bus">
				<span style="float:left;width:60px;text-align: right;">
					<a href="javascript:;" class="bus-num individual <?=$b['cat']?>" data-num="<?=$b['num']?>" data-stop="<?=$b['stop']?>"><?=$b['num']?></a>
				</span>
				<span class="description" style="float:left;">
					<?=$b['stop']?><br>
					<?=$b['desc']?>
				</span>

				<span style="float:right;margin-top:15px;">
					<span class="time" id="next_<?=$b['num'],'_',$b['stop']?>" data-arrival_ts=""></span>
					<span class="time" style="padding-left:5px" id="subs_<?=$b['num'],'_',$b['stop']?>" data-arrival_ts=""></span>
				</span>
				<div class="clearfix"></div>
			</div>
		<?php } ?>

		<br><br>

		<div style="text-align:center">
			<button type="button" class="load_to_work">To Work</button>
			<button type="button" class="from_work">From Work</button>
		</div>

		<div style="margin-top:10px;">
			Bus No.  <input type="tel" name="bus_num" id="custom_bus" style="width:20%;" />
			Stop No. <input type="tel" name="stop_num" id="custom_stop" style="width:20%;" />

			<button type="button" id="custom_query">Query</button>
		</div>

		<div style="margin-top:10px;">
			Status: <span id="status"></span>
		</div>

		<script>
			var timer_started = false;

			$(document).ready(function() {
				$('a.individual').on('click', function() {
					var butt = $(this);
					var num = butt.attr('data-num');
					var stop = butt.attr('data-stop');
					var ts = Math.round((new Date).getTime() / 1000);

					$('#status').html('Loading');
					$.get(
						'http://wyred.nu/iris.php?bus_num='+num+'&bus_stop='+stop,
						function(resp) {
							if (resp.success) {
								$('#status').html('Ok');
								$('#next_'+num+'_'+stop).html(resp.next);
								$('#subs_'+num+'_'+stop).html(resp.subsequent);
								$('#next_'+num+'_'+stop).attr('data-arrival_ts', ts + (resp.next * 60));
								$('#subs_'+num+'_'+stop).attr('data-arrival_ts', ts + (resp.subsequent * 60));
								countdown();
							} else {
								$('#status').html('Not ok');
								$('#next_'+num+'_'+stop).html('err');
								$('#subs_'+num+'_'+stop).html('err');
							}
						},
						'json'
					).error(function() {
						$('#status').html('Server error');
					});
				});

				$('button.from_work').on('click', function() {
					$('a.from_work').click();
				});
				$('button.load_to_work').on('click', function() {
					$('a.to_work').click();
				});

				$('#custom_query').on('click', function() {
					var bus = $('#custom_bus').val();
					var stop = $('#custom_stop').val();
					var result = iris_query(stop, bus);

					if (result.success) {
						$('#status').html('Next: '+result.next+' - Sub: '+result.subsequent);
					} else {
						$('#status').html('Server error');
					}
				})
			});

			function countdown() {
				$('.time').each(function() {
					var ts = Math.round((new Date).getTime() / 1000);
					td = $(this);
					if (td.attr('data-arrival_ts') != '') {
						arrival_ts = td.attr('data-arrival_ts');
						var diff = arrival_ts - ts;
						if (diff < 0) {
							td.html(0);
						} else {
							td.html(secondsToTime(diff));
						}
					}
				});

				if (!timer_started) {
					setInterval(function(){countdown()},1000);
					timer_started = true;
				}
			}

			function secondsToTime(secs) {
				var divisor_for_minutes = secs % (60 * 60);
				var minutes = Math.floor(divisor_for_minutes / 60);

				var divisor_for_seconds = divisor_for_minutes % 60;
				var seconds = Math.ceil(divisor_for_seconds);

				if (seconds < 10) seconds = '0'+seconds;
				return minutes+':'+seconds;
			}

			function iris_query(stop, bus) {
				var query_result = new Object();

				$.ajax({
					url: 'http://wyred.nu/iris.php?bus_num='+bus+'&bus_stop='+stop,
					type: 'get',
					dataType: 'json',
					async: false,
					success: function(resp) {
						query_result = resp;
					},
					error: function() {
						query_result.success = false;
					}
				});

				return query_result;
			}
		</script>
	</body>
</html>